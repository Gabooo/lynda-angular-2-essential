import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MediaItemComponent } from './media-item/media-item.component';
import { MediaItemService } from './media-item/media-item.service';

import { FirstPipe } from './pipes/first.pipe';

@NgModule({
  //are to make directives (including components and pipes) from the current module available to other directives in the current module.
  declarations: [
    AppComponent,
    MediaItemComponent,
    FirstPipe
  ],
  //makes the exported declarations of other modules available in the current module
  imports: [
    BrowserModule,
    AppRoutingModule,
    //Not to declaration
    FormsModule
  ],
  //are to make services and values known to DI (dependency injection)
  providers: [MediaItemService],
  bootstrap: [AppComponent]
})
export class AppModule { }
