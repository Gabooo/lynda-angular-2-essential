import { Pipe } from '@angular/core';

@Pipe({
    name: 'firstPipe'
})
export class FirstPipe {
    //Pipe has to contain transform function
    transform(data:string) {
        console.log('data', data);
        return data.length
    }
}