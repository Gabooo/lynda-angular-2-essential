import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MyText} from './../types/text.class';
import { MediaItemService } from './media-item.service';

@Component({
  selector: 'app-media-item',
  templateUrl: './media-item.component.html',
  styleUrls: ['./media-item.component.scss']
})
export class MediaItemComponent implements OnInit {
  @Input() mediaItem : MyText;
  @Output() delete = new EventEmitter();

  constructor(
    private mediaService: MediaItemService
  ){}

  ngOnInit() {
    this.mediaItem = this.mediaService.get();
    console.log('media item from service', this.mediaItem)
  }

  onDelete() {
    this.delete.emit(this.mediaItem);
    console.log('delete');
  }

  onSubmit(data) {
    console.log('form data', data);
  }

}
